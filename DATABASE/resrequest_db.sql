-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 17, 2015 at 10:58 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `resrequest`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reservations`
--

CREATE TABLE `tbl_reservations` (
`reservation_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `num_of_rooms` int(11) NOT NULL,
  `reference_code` varchar(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tel_number` varchar(25) NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_reservations`
--

INSERT INTO `tbl_reservations` (`reservation_id`, `venue_id`, `date_from`, `date_to`, `num_of_rooms`, `reference_code`, `username`, `email`, `tel_number`, `confirmed`, `date_created`, `active`) VALUES
(3, 1, '2015-08-17', '2015-08-19', 1, '199612089e', 'Bradley', 'brad@mooidev.co.za', '+27842888001', 0, '2015-08-16 19:03:59', 1),
(4, 1, '2015-08-16', '2015-08-17', 1, '3e1a553301', 'Bradley', 'brad@mooidev.co.za', '+27842888001', 0, '2015-08-16 19:21:12', 1),
(5, 1, '2015-08-16', '2015-08-17', 1, 'cf222bae29', 'Brad', 'brad@mooidev.co.za', '0842888001', 0, '2015-08-16 19:32:04', 1),
(6, 1, '2015-08-17', '2015-08-18', 1, 'a0a9743aa5', 'Bradley', 'brad@mooidev.co.za', '+27842888001', 0, '2015-08-17 05:35:54', 1),
(7, 1, '2015-08-16', '2015-08-29', 1, '7f5618fce1', 'Brad Corden', 'brad@mooidev.co.za', '+27842888001', 0, '2015-08-17 20:38:54', 1),
(8, 1, '2015-08-20', '2015-08-30', 2, '15327a7887', 'Bradley', 'brad@mooidev.co.za', '+27842888001', 0, '2015-08-17 20:42:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_venues`
--

CREATE TABLE `tbl_venues` (
`venue_id` int(11) NOT NULL,
  `venue_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tel_num` varchar(25) NOT NULL,
  `rooms_available` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_venues`
--

INSERT INTO `tbl_venues` (`venue_id`, `venue_name`, `email`, `tel_num`, `rooms_available`, `date_created`, `active`) VALUES
(1, 'Demo Venue', 'bookings@demovenue.co.za', '011 777 8888', 50, '2015-08-15 22:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_reservations`
--
ALTER TABLE `tbl_reservations`
 ADD PRIMARY KEY (`reservation_id`), ADD UNIQUE KEY `reservation_id` (`reservation_id`), ADD KEY `reservation_id_2` (`reservation_id`), ADD KEY `reservation_id_3` (`reservation_id`), ADD KEY `reservation_id_4` (`reservation_id`);

--
-- Indexes for table `tbl_venues`
--
ALTER TABLE `tbl_venues`
 ADD PRIMARY KEY (`venue_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_reservations`
--
ALTER TABLE `tbl_reservations`
MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_venues`
--
ALTER TABLE `tbl_venues`
MODIFY `venue_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
