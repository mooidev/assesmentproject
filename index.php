<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="From Star Wars geeks to Pixar fans and adrenalin junkies, we’re a pretty lively bunch at ResRequest. What we have in common (besides our good looks and great personalities!) is our passion for developing an awesome product and supporting some of the tourism industry’s most inspiring people and most wished-for holiday spots.">
<meta name="keywords" content="Tourism, Reservations, Bookings, Travel, Accomodation, Product, Support, Software, Integration, API">
<meta name="author" content="Brad Corden">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>ResRequest Bookings</title>
<link rel="icon" href="images/favicon.ico">

<!-- CSS Style Sheets -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/responsive.css">
</head>

<body>

<div class="preloader">
  <div class="status">&nbsp;</div>
</div>

<div class="wrapper">

  <div class="color-overlay">
    
    <div class="container">
      
      <div class="row">
	
	<!-- PAGE WELCOME CONTENT -->
	<div class="col-md-7 col-sm-7 intro-section">
	  
	  <h1 class="intro text-left">
	  <strong>ResRequest</strong> the Best <strong>Reservation Solution</strong> known to <strong>Travelers</strong>
	  </h1>
	  
	  <ul class="feature-list-1">
	    
	    <li>
	    <div class="icon-container pull-left">
	      <span class="icon_check"></span>
	    </div>
	    <p class="text-left">
	      Please fill out our online reservation form, and one of our friendly agents will contact you within 24 hours. 
	    </p>
	    </li>
	    <li>
	    <div class="icon-container pull-left">
	      <span class="icon_check"></span>
	    </div>
	    <p class="text-left">
	      Please note that all requests will be final once we have received your deposit. Deposits need to be paid to us within 48 hours of request.
	    </p>
	    </li>
	    <li>
	    <div class="icon-container pull-left">
	      <span class="icon_check"></span>
	    </div>
	    <p class="text-left">
	      We will do everything in our power to make your stay as comfortable and stress free as possible.
	    </p>
	    </li>
	  </ul>
	</div>
	      
	  <!-- RIGHT - BOOKING FORM -->
	  <div class="col-md-5 col-sm-5">
	    
	    <div class="vertical-registration-form">
	      
	      <h2>Make a Reservation</h2>
	      <form class="registration-form" id="registration-form" role="form">
		<input class="form-control input-box" id="name" type="text" name="name" placeholder="Your Name" required>
		<input class="form-control input-box" id="email" type="email" name="email" placeholder="Your Email" required>
		<input class="form-control input-box" id="phone" type="text" name="phone" placeholder="Phone Number">
		<input class="form-control input-box" id="from" type="text" name="from" placeholder="Choose date from" required>
		<input class="form-control input-box" id="to" type="text" name="to" placeholder="Choose date to" required>
		<input class="form-control input-box" id="rooms" type="number" name="rooms" placeholder="Number of Rooms" required>
		<button type="submit" class="btn standard-button" id="submit" name="submit">Send Reservation</button> 
	      </form>
	    </div>
	  </div>
	  <!-- /END - BOOKING FORM -->
      </div>
    </div>
  </div>
</div>

<!-- Javascript libraries -->
<script src="js/jquery.min.js"></script>
<!-- Custom JS file-->
<script src="js/general.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Bootstrap Date Picker Includes-->
<script src="js/moment.min.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
</body>
</html>