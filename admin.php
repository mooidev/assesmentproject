<?php
// Json include file.
require("json.php");
?>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="From Star Wars geeks to Pixar fans and adrenalin junkies, we’re a pretty lively bunch at ResRequest. What we have in common (besides our good looks and great personalities!) is our passion for developing an awesome product and supporting some of the tourism industry’s most inspiring people and most wished-for holiday spots.">
<meta name="keywords" content="Tourism, Reservations, Bookings, Travel, Accomodation, Product, Support, Software, Integration, API">
<meta name="author" content="Brad Corden">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>ResRequest Booking Results</title>
<link rel="icon" href="images/favicon.ico">

<!-- CSS Style Sheets -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/responsive.css">
</head>

<body>

<div class="preloader">
  <div class="status">&nbsp;</div>
</div>

<div class="wrapper">

  <div class="color-overlay">
    
    <div class="container">
      
      <div class="row">
	  <!-- LEFT - SEARCH FORM -->
	  <div class="col-md-5 col-sm-5">
	    
	    <div class="vertical-search-form">
	      
	      <h2>Reservation Search</h2>
	      <input class="form-control input-box" id="reference" type="text" name="reference" placeholder="Reference Code" data-provide="typeahead">
	      <h4>OR</h4>
	      <input class="form-control input-box" id="from" type="text" name="from" placeholder="Choose date from" required>
	      <input class="form-control input-box" id="to" type="text" name="to" placeholder="Choose date to" required>
	      <button type="submit" class="btn standard-button" id="searchBtn" name="searchBtn">Search Reservations</button> 
	    </div>
	  </div>
	  <!-- /END - SEARCH FORM -->

	<!-- SEARCH RESULT CONTENT -->
	<div class="col-md-7 col-sm-7 intro-section">
	  
	  <h1 class="intro text-left">
	  <strong>Search</strong> results...
	  </h1>
	  
	  <ul class="feature-list-1" style="visibility: hidden">
	    
	    <!-- RESERVATION RESULTS WILL BE PRINTED HERE -->
	    
	  </ul>
	</div>
	      
      </div>
    </div>
  </div>
</div>

<!-- Javascript libraries -->
<script src="js/jquery.min.js"></script>
<!-- Custom JS file-->
<script src="js/general.js"></script>
<script>
  //Get reference code Json.
  $.get('resdata.json', function(data){
    $("#reference").typeahead({
      source:data,
      updater:function (item) {
	// Function call on autoComplete select.
	ResRequest.getReservationDetails(item);
        return item;
    }
    });
  },'json');
</script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Bootstrap Date Picker Includes-->
<script src="js/moment.min.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<!-- Bootstrap Autocomplete Includes-->
<script src="js/bootstrap3-typeahead.min.js"></script>
</body>
</html>