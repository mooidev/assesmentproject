<?php

// Database include file.
require("dbconnect.php");

if (isset($_POST['getResDetails'])) {
    
    $referenceCode = $_POST['rc'];
    
    // MySQL select to get reservation details.
    $sql = "SELECT tbl_reservations.*, tbl_venues.venue_name
            FROM tbl_reservations
            INNER JOIN tbl_venues ON tbl_venues.venue_id = tbl_reservations.venue_id
            WHERE reference_code = '".$referenceCode."'";
    $result = mysql_query($sql, $conn);
    
    // Create JSON array.
    $resarray = [];
    while ($arr = mysql_fetch_assoc($result)) {
        
        $jsonArrayObject = (array('venue' => $arr["venue_name"], 'from' => $arr["date_from"], 'to' => $arr["date_to"],
            'rooms' => $arr["num_of_rooms"], 'name' => $arr["username"], 'email' => $arr["email"],
            'number' => $arr["tel_number"]));
        $resarray[] = $jsonArrayObject;
    }
    
    // Return JSON array.
    $json_array = json_encode($resarray);
    echo $json_array;
    
} elseif (isset($_POST['getReslist'])) {

    $from = $_POST['from'];
    $to = $_POST['to'];
    
    // MySQL select to get reservation details.
    $sql = "SELECT tbl_reservations.*, tbl_venues.venue_name
            FROM tbl_reservations
            INNER JOIN tbl_venues ON tbl_venues.venue_id = tbl_reservations.venue_id
            WHERE date_from >= '".$from."' AND date_to <= '".$to."'
            ORDER BY date_from ASC";
    $result = mysql_query($sql, $conn);
    
    // Create JSON array.
    $resarray = [];
    while ($arr = mysql_fetch_assoc($result)) {
        
        $jsonArrayObject = (array('venue' => $arr["venue_name"], 'from' => $arr["date_from"], 'to' => $arr["date_to"],
            'rooms' => $arr["num_of_rooms"], 'name' => $arr["username"], 'email' => $arr["email"],
            'number' => $arr["tel_number"]));
        $resarray[] = $jsonArrayObject;
    }
    
    // Return JSON array.
    $json_array = json_encode($resarray);
    echo $json_array;

} else {
    
    // MySQL select for reference code autocomplete (Init).
    $sql = "SELECT reference_code
            FROM tbl_reservations
            ORDER BY date_created DESC";
    $result = mysql_query($sql, $conn);
    
    // Create JSON array.
    $resarray = array();
    while ($arr = mysql_fetch_assoc($result)) {
        
        $resarray[] = $arr['reference_code'];
    }
    
    // Write JSON file.
    $logFile = 'resdata.json';
    $fh = fopen($logFile, 'a');
    $the_date_is = date('Y-m-d H:i:s');
    fwrite($fh, $the_date_is."\n\n".json_encode($resarray)."\n\n");
    fclose($fh);
}
?>