<?php

// Database include file.
require("dbconnect.php");
require('class.phpmailer.php');

// Define variables
$fullname   = $_POST["name"];
$splitname  = explode(" ", $fullname);
$firstname  = $splitname[0];
$email      = $_POST["email"];
$phone      = $_POST["phone"];
$from       = $_POST["from"];
$to         = $_POST["to"];
$rooms      = $_POST["rooms"];

function random_ref($bytes) {
    
    $rand = mcrypt_create_iv($bytes, MCRYPT_DEV_URANDOM);
    return bin2hex($rand);
}
$refnumber = random_ref(5);

// Submit client booking request.
$sql = "INSERT INTO tbl_reservations (
            venue_id,
            date_from,
            date_to,
            num_of_rooms,
            reference_code,
            username,
            email,
            tel_number)
        VALUES (
            1,
            '".$from."',
            '".$to."',
            '".$rooms."',
            '".$refnumber."',
            '".$fullname."',
            '".$email."',
            '".$phone."')";
$result = mysql_query($sql, $conn);

// Send confirmation email to customer.
$mail = new PHPMailer(true);
$mail->IsSendmail();
$mail->IsHTML(true);
$mail->Priority = 1;
$mail->From = "bookings@rerequest.co.za";
$mail->FromName = "ResRequest online bookings";
$mail->AddAddress($email);
$mail->Subject = "ResRequest online booking - REF# '.$refnumber.'.";

$message = "<html>"."\r\n";
$message .= "<head>"."\r\n";
$message .= "	<title>ResRequest</title>"."\r\n";
$message .= "	<style type=\"text/css\">";
$message .= "		.default		{ color:#333333; font-size:12px; font-family:Tahoma; line-height:14px; }";
$message .= "	</style>"."\r\n";
$message .= "</head>"."\r\n";
$message .= "<body marginwidth=0 marginheight=0 topmargin=0 leftmargin=0>"."\r\n";
$message .= "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"default\">"."\r\n";
$message .= "	<tr>"."\r\n";
$message .= "		<td width=10 height=1></td>"."\r\n";
$message .= "		<td width=590>Hi '.$firstname.'!<br /><br /></td>"."\r\n";
$message .= "	</tr>"."\r\n";
$message .= "	<tr>"."\r\n";
$message .= "		<td width=* height=1></td>"."\r\n";
$message .= "		<td width=*>Thank you for using ResRequest for your travel requirements. We will be in contact with you shortly regarding your booking request.<br /><br />Please refer to your reference number ('.$refnumber.') for all further communication.<br /><br /></td>"."\r\n";
$message .= "	</tr>"."\r\n";
$message .= "	<tr>"."\r\n";
$message .= "		<td width=* height=1></td>"."\r\n";
$message .= "		<td width=*>Thanks again,<br /><br />ResRequest.</td>"."\r\n";
$message .= "	</tr>"."\r\n";
$message .= "</table>"."\r\n";
$message .= "</body>"."\r\n";
$message .= "</html>";

$mail->Body = $message;
$mail->Send();

// Display thank you message.
echo '<h2>THANK YOU '.$firstname.'!</h2><br />Your request has been logged and will be attended to shortly.<br /><br />Kind Regards,<br /><br />ResRequest.<br><br><button type="button" class="btn standard-button" id="back" name="back" onclick="javascript:location.reload();">Make Another Reservation</button> ';
exit;
?>