// makes sure the whole site is loaded
$(window).load(function() {
    "use strict";
    // will first fade out the loading animation
    $(".status").fadeOut();
    // will fade out the whole DIV that covers the website.
    $(".preloader").delay(1000).fadeOut("slow");
});

// submit form via ajax
$('form').submit(function(e) {
    
  e.preventDefault();
  $.ajax({
        type: 'POST',
        url: 'bookings.php',
        data: $('#registration-form').serialize(),
        success: function(msg) {
            $('.vertical-registration-form').fadeOut(500, function(){
                $('.vertical-registration-form').html(msg).fadeIn();
            });
        }
    });  
  return false; 
});               

var ResRequest = ResRequest || {};

ResRequest.getReservationDetails = function(resCode) {
  
  var theData = 'getResDetails=y&rc=' + resCode;
  $.ajax({
    type: 'POST',
    url: 'json.php',
    data: theData,
    success: function(json) {
      
      $(".feature-list-1").attr("style", "visibility: visible");
      ResRequest.resDetailArray = JSON.parse(json);
      
      var resData = '<li>';
      resData += '<p class="text-left">';
      resData += '<table cellpadding=2 cellspacing=2 border=0 class="resDetailTbl">';
      resData += '<tr>';
      //resData += '<th>Venue</th>';
      resData += '<th>From</th>';
      resData += '<th>To</th>';
      resData += '<th># Rooms</th>';
      resData += '<th>Name</th>';
      resData += '<th>Email</th>';
      resData += '<th>Phone</th>';
      resData += '</tr>';
      resData += '<tr>';
      $.each(ResRequest.resDetailArray, function( key, value ) {
	  //resData += '<td>' + value.venue + '</td>';
	  resData += '<td>' + value.from + '</td>';
	  resData += '<td>' + value.to + '</td>';
	  resData += '<td>' + value.rooms + '</td>';
	  resData += '<td>' + value.name + '</td>';
	  resData += '<td><a href="mailto:' + value.email + '">' + value.email + '</a></td>';
	  resData += '<td>' + value.number + '</td>';
      });
      resData += '</tr>';
      resData += '</table>';
      resData += '</p>';
      resData += '</li>';
      
      $('.feature-list-1').html(resData);
    }
  });  
}

ResRequest.getReservationList = function() {
  
  var from = $('#from').val();
  var to = $('#to').val();

  var theData = 'getReslist=y&from=' + from + '&to=' + to;
  $.ajax({
    type: 'POST',
    url: 'json.php',
    data: theData,
    success: function(json) {
      
      $(".feature-list-1").attr("style", "visibility: visible");
      ResRequest.resDetailArray = JSON.parse(json);
      
      var resData = '<li>';
      resData += '<p class="text-left">';
      resData += '<table cellpadding=2 cellspacing=2 border=0 class="resDetailTbl">';
      resData += '<tr>';
      //resData += '<th>Venue</th>';
      resData += '<th>From</th>';
      resData += '<th>To</th>';
      resData += '<th># Rooms</th>';
      resData += '<th>Name</th>';
      resData += '<th>Email</th>';
      resData += '<th>Phone</th>';
      resData += '</tr>';
      
      if (ResRequest.resDetailArray.length == 0) {
	
	resData += '<tr>';
	resData += '<td colspan=5><h3>Oops! No reservations found.</h3></td>';
	resData += '</tr>';
      } else {
	
	$.each(ResRequest.resDetailArray, function( key, value ) {
	    resData += '<tr>';
	    //resData += '<td>' + value.venue + '</td>';
	    resData += '<td>' + value.from + '</td>';
	    resData += '<td>' + value.to + '</td>';
	    resData += '<td>' + value.rooms + '</td>';
	    resData += '<td>' + value.name + '</td>';
	    resData += '<td><a href="mailto:' + value.email + '">' + value.email + '</a></td>';
	    resData += '<td>' + value.number + '</td>';
	    resData += '</tr>';
	});
      }
      resData += '</table>';
      resData += '</p>';
      resData += '</li>';
      
      $('.feature-list-1').html(resData);
    }
  });
}

$(document).ready(function () {
  
  // date time picker
  $('#from').datetimepicker({
    format: 'YYYY-MM-DD'
  });
  $('#to').datetimepicker({
    format: 'YYYY-MM-DD',						
    useCurrent: false //Important! See issue #1075
  });
  $("#from").on("dp.change", function (e) {
    $('#to').data("DateTimePicker").minDate(e.date);
  });
  $("#to").on("dp.change", function (e) {
    $('#from').data("DateTimePicker").maxDate(e.date);
  });
  
  //Function call when clicking on search button.
  $(document).on('click', '#searchBtn', ResRequest.getReservationList);
   
});